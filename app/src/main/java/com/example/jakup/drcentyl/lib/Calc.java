package com.example.jakup.drcentyl.lib;

import android.os.Bundle;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import static java.lang.Math.abs;
import static java.lang.Math.min;
import static java.lang.Math.round;

/**
 * Created by jkijo on 30.10.2017.
 */


public class Calc {

    //function for calculating exact value from centyl chart on the basis of Poly class
    //Uses reflection to get needed function -> "prefix + what + "18_" + centyl[i]"
    //Allowed "what" variables are Height, Weight, Bmi, Head  // TODO I GUESS WE HAVE NOTHING FOR PRESSURE
    //Allowed sex are 'girls', 'boys'



    public static double[] Calculate(double age, boolean sex, String what)
    {
        Poly p = new Poly();
        p.boysHead18_25(12.44);

        double [] tab = {0,0,0,0,0,0,0};
        int [] centyl = {3, 10, 25, 50, 75, 90, 97};
        String prefix = new String();

        if(sex==true)
            prefix = "boys";
        else
            prefix = "girls";

        if(what == "Bmi" && age < 4.0)
        {
            return tab;
        }
        else
        {
            if(age>=1)
            {
                for (int i =0; i<7 ; i++)
                {
                    Method m = null;
                    try {
                        m = p.getClass().getDeclaredMethod(prefix + what + "18_" + centyl[i], Double.TYPE);
                    } catch (NoSuchMethodException e) {
                        e.printStackTrace();
                    }
                    try {
                        tab[i]=(double) m.invoke(null, age);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        e.printStackTrace();
                    }
                }

            }
            //children 1-11 months
            else
            {
                //dziedziną są miesiące
                age= age*12;

                for (int i =0; i<7 ; i++)
                {
                    Method m = null;
                    try {
                        m = p.getClass().getDeclaredMethod(prefix + what + "12_" + centyl[i], Double.TYPE);
                    } catch (NoSuchMethodException e) {
                        e.printStackTrace();
                    }
                    try {
                        tab[i]=(double) m.invoke(null, age);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        return tab;
    }


    public static int Centyl (double[] tab, double input)
    {
        double diff=100000000;
        double temp;
        double fractionOfDist;
        int winning=0;
        int winning2=0;
        int centyl_specific;
        ArrayList<Double> list = new ArrayList<>();
        int [] centyl = {3, 10, 25, 50, 75, 90, 97};

        for (int i= 0; i<7; i++)
            list.add(abs(tab[i]-input));

        //The best result
        for (int i= 0; i<7; i++)
        {
            temp = list.get(i);
            if (temp < diff) {
                diff = temp;
                winning = i;
            }
        }

        //Preparing for calculation of 2nd best
        list.set(winning, 10000.0);
        diff = 10000;

        //Second best
        for (int i= 0; i<7; i++)
        {
            temp = list.get(i);
            if (temp < diff) {
                diff = temp;
                winning2 = i;
            }
        }

        //eg. centyl 3 -> 152cm and centyl 10 160cm. So for 154cm f_o_d= 154-152 / 8   =   0.25
        fractionOfDist = (input-min(tab[winning],tab[winning2]))/(abs(tab[winning]-tab[winning2]));
        centyl_specific =(int) round(min(centyl[winning],centyl[winning2]) + fractionOfDist*(abs(centyl[winning]-centyl[winning2])));

        if (centyl_specific <3)
            centyl_specific = 3;
        if (centyl_specific >97)
            centyl_specific = 97;


        return centyl_specific;

    }
}


//---------------------------------------------WERSJA DZIEWCZYN---------------------------
//package com.example.jakup.drcentyl.lib;
//
//import android.os.Bundle;
//
//import java.lang.reflect.InvocationTargetException;
//import java.lang.reflect.Method;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.List;
//
//import static java.lang.Math.abs;
//
///**
// * Created by jkijo on 30.10.2017.
// */
//
//
//public class Calc {
//
//    //function for calculating exact value from centyl chart on the basis of Poly class
//    //Uses reflection to get needed function -> "prefix + what + "18_" + centyl[i]"
//    //Allowed "what" variables are Height, Weight, Bmi, Head  // TODO I GUESS WE HAVE NOTHING FOR PRESSURE
//    //Allowed sex are 'girls', 'boys'
//
//
//
//    public static double[] Calculate (double age, boolean sex, String what)
//    {
//        Poly p = new Poly();
//        p.boysHead18_25(12.44);
//
//        double [] tab = {0,0,0,0,0,0,0};
//        int [] centyl = {3, 10, 25, 50, 75, 90, 97};
//        String prefix = new String();
//
//        if(sex==true)
//            prefix = "boys";
//        else
//            prefix = "girls";
//
//        if(what == "Bmi" && age < 4.0)
//        {
//            return tab;
//        }
//        else
//        {
//            if(age>=1)
//            {
//                for (int i =0; i<7 ; i++)
//                {
//                    Method m = null;
//                    try {
//                        m = p.getClass().getDeclaredMethod(prefix + what + "18_" + centyl[i], Double.TYPE);
//                    } catch (NoSuchMethodException e) {
//                        e.printStackTrace();
//                    }
//                    try {
//                        tab[i]=(double) m.invoke(null, age);
//                    } catch (IllegalAccessException e) {
//                        e.printStackTrace();
//                    } catch (InvocationTargetException e) {
//                        e.printStackTrace();
//                    }
//                }
//
//            }
//            //children 1-11 months
//            else
//            {
//                //dziedziną są miesiące
//                age= age*12;
//
//                for (int i =0; i<7 ; i++)
//                {
//                    Method m = null;
//                    try {
//                        m = p.getClass().getDeclaredMethod(prefix + what + "12_" + centyl[i], Double.TYPE);
//                    } catch (NoSuchMethodException e) {
//                        e.printStackTrace();
//                    }
//                    try {
//                        tab[i]=(double) m.invoke(null, age);
//                    } catch (IllegalAccessException e) {
//                        e.printStackTrace();
//                    } catch (InvocationTargetException e) {
//                        e.printStackTrace();
//                    }
//                }
//
//            }
//        }
//
//        return tab;
//    }
//
//    public static ArrayList<double[]> CalculateWithInterpolation (double age, boolean sex, String what)
//    {
//        ArrayList<double[]> tabs = new ArrayList<double[]>();
//        Poly p = new Poly();
//        p.boysHead18_25(12.44);
//
//        double [] tab = {0,0,0,0,0,0,0};
//        double [] tabAgePlus = {0,0,0,0,0,0,0};
//        double [] ageArray = {0,0, 0};
//        int [] centyl = {3, 10, 25, 50, 75, 90, 97};
//        String prefix = new String();
//
//        if(sex==true)
//            prefix = "boys";
//        else
//            prefix = "girls";
//
//        if(what == "Bmi" && age < 4.0)
//        {
//            return tabs;
//        }
//        else
//        {
//            if(age>=1)
//            {
//                ageArray[0] = age;
//                age = roundToHalfs(age, 1);
//                for (int i =0; i<7 ; i++)
//                {
//                    Method m = null;
//                    try {
//                        m = p.getClass().getDeclaredMethod(prefix + what + "18_" + centyl[i], Double.TYPE);
//                    } catch (NoSuchMethodException e) {
//                        e.printStackTrace();
//                    }
//                    try {
//                        tab[i]=(double) m.invoke(null, age);
//                        tabAgePlus[i]=(double) m.invoke(null, age+0.5);
//                        ageArray[1] = age;
//                        ageArray[2] = age + 0.5;
//                    } catch (IllegalAccessException e) {
//                        e.printStackTrace();
//                    } catch (InvocationTargetException e) {
//                        e.printStackTrace();
//                    }
//                }
//
//            }
//            //children 1-11 months
//            else
//            {
//                //dziedziną są miesiące
//                age= age*12;
//                ageArray[0] = age;
//                age = round(age,0);
//
//                for (int i =0; i<7 ; i++)
//                {
//                    Method m = null;
//                    try {
//                        m = p.getClass().getDeclaredMethod(prefix + what + "12_" + centyl[i], Double.TYPE);
//                    } catch (NoSuchMethodException e) {
//                        e.printStackTrace();
//                    }
//                    try {
//                        tab[i]=(double) m.invoke(null, age);
//                        tabAgePlus[i]=(double) m.invoke(null, age+1);
//                        ageArray[0] = age;
//                        ageArray[1] = age + 1;
//                    } catch (IllegalAccessException e) {
//                        e.printStackTrace();
//                    } catch (InvocationTargetException e) {
//                        e.printStackTrace();
//                    }
//                }
//
//            }
//        }
//
//        tabs.add(tab);
//        tabs.add(tabAgePlus);
//        tabs.add(ageArray);
//
//        return tabs;
//    }
//
//    //function for getting centyl from exact value calculated in Calc.Calculate
//    //So the invocation can be nested like in DisplayChartsActivity
//    public static double Centyl (ArrayList<double[]> tabs, double input)
//    {
//        double[] tab = tabs.get(0);
//        double[] tabAgePlus = tabs.get(1);
//        double[] ageArray = tabs.get(2);
//        double age = ageArray[0];
//        double agePlus = ageArray[2];
//        double realAge = ageArray[1];
//
//        double diff=100000000;
//        double diff2=100000000;
//        double temp;
//        int winning=0;
//        int smallest = 0;
//        int [] centyl = {3, 10, 25, 50, 75, 90, 97};
//        Double[] tempValues = new Double[7];
//
//        //    3. Wyznaczamy wartość funkcji dla centyli (3,10,25,50,75,90,97) dla dzieci w wieku 8.8, czyli:
////        - bierzemy wartości z wieku 8.5 (albo 8, nie wiem jak dokładnie tablicowaliście wykresy, załóżmy, że 8), oznaczmy je jako W(8,3), W(8,10), W(8,25)...   W(wiek, centyl)
////            - bierzemy wartości z wieku 9, oznaczmy je jako W(9,3), W(9,10), W(9,25)...
////        - wartości funkcji dla wieku 8.8 interpolujemy następująco:
////        W(8.8,3) = W(8,3) + (8.8 - 8) / (9-8)  * (W(9,3) - W(8,3)) = 7.0 + 0.8 / 1 * 0.3 = 7.24
////                - podobnie wyznaczamy wartości W(8.8, 10), W(8.8, 25)...
////        for (int i= 0; i<7; i++)
////        {
//////            temp = abs(tab[i]-input);
//////            tempValues[i] = tab[i] + (realAge-age)/(agePlus-age) * (tabAgePlus[i] - tab[i]);
////            tempValues[i] = tab[i] + (realAge - age) / (agePlus - age) * (tabAgePlus[i] - tab[i]);
////            temp = abs(tempValues[i] - input);
////            if (temp < diff) {
////                diff = temp;
////                winning = i;
////            }
////            if(tempValues[i]< diff2) {
////                diff2 = tempValues[i];
////                smallest = i;
////            }
////
////        }
//
//        for (int i = 0; i < 7; i++)
//        {
//            tempValues[i] = tab[i] + (realAge - age) / (agePlus - age) * (tabAgePlus[i] - tab[i]);
//            if (tempValues[i] < input)
//            {
//                smallest = i;
//            }
//
//        }
//
//        double winningTemp = tempValues[winning];
//        double secondWinTemp;
//        double diff3 = 10000;
//        int smallestTwo = 0;
//
//        for (int j=0; j < 7; j++) {
//            if(tempValues[j] > diff2 && tempValues[j]< diff3) {
//                diff3 = tempValues[j];
//                smallestTwo = j;
//            }
//        }
//
//        //        4. Sprawdzamy, pomiędzy którymi funkcjami jest waga 10 kg. Wychodzi, że między 90 a 97 centylem (krok 3 i 4 można uprościć, to jest wiadomo - łopatologiczne rozwiązanie)
////        5. Wyznaczamy centyl, czyli daną wyjściową C:
////    C = 90 + (10 - W(8.8,90))/(W(8.8,97)-W(8.8,90)) * (97-90) = ok. 94 centyl
//        double winningCentyl;
//        if (smallest < smallestTwo) {
//            winningCentyl = centyl[smallest] + (input - tab[smallest])/(tab[smallestTwo] - tab[smallest]) * (centyl[smallestTwo] - centyl[smallest]);
//        } else {
//            winningCentyl = centyl[smallestTwo] + (input - tab[smallestTwo])/(tab[smallest] - tab[smallestTwo]) * (centyl[smallest] - centyl[smallestTwo]);
//        }
//
//       // winningCentyl = centyl[smallest] + (input - tab[smallest]) / (tab[smallest + 1] - tab[smallest]) * (centyl[smallest + 1] - centyl[smallest]);
//
//        return round(winningCentyl, 2);//centyl[winning];
//
//    }
//
//    public static double round(double value, int places) {
//        if (places < 0) throw new IllegalArgumentException();
//        long factor = (long) Math.pow(10, places);
//        value = value * factor;
//        long tmp = Math.round(value);
//        return (double) tmp / factor;
//    }
//
//    public static double roundToHalfs(double value, int places) {
//        if (places < 0) throw new IllegalArgumentException();
//int roundedDown = (int)value;
//        double diff = value - roundedDown;
//        double res;
//        double diffToCompare = 0.5;
//
//        if(diff >= diffToCompare) {
//
//            res = roundedDown + 0.5;
//        } else {
//            res = roundedDown;
//        }
//
//
//        return (double) res;
//    }
//}
